#ifndef _USART_H
#define _USART_H

#include <stdio.h>
#include "stm32f2xx.h"
#include "config.h"

//#define OpenUSART1

//#define Open207Z_USART1_TX_GPIO_PORT GPIOA
//#define Open207Z_USART1_TX_PIN  GPIO_Pin_9
//#define Open207Z_USART1_CLK RCC_AHB1Periph_GPIOA

//#define Open207Z_USART1_RX_GPIO_PORT GPIOA
//#define Open207Z_USART1_RX_PIN  GPIO_Pin_10

void USART_Configuration(void);
void USART_NVIC_Config(void);

#endif /*_USART_H*/
